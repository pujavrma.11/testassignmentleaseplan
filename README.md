# TestAssignmentLeasePlan(Cucumber)

**This is automation suite is designed to test the Swagger Pet store API with POST,GET & DELETE method** 

The tech stack used for this project are:

1. JAVA as the programming language for writing test code
2. Cucumber as the framework
3. Eclipse as the preferred IDE for writing java code. 
4. JDK1.8
 

**Cloning & Importing the Project**

- Clone the project from git clone [here](https://gitlab.com/pujavrma.11/testassignmentleaseplan.git) 

**Running tests**

Note: For 2nd steps, Need to follow this way `CMD -> cd <change-to-project-location>`
1. Test can run the tests directly from the eclipse, by right-clicking from TestRunnerPet.java class and Run as Junittest.
2. For Windows users: `mvn test verify`
3. Run the pipeline [here](https://gitlab.com/pujavrma.11/testassignmentleaseplan/-/pipelines) to run the automated tests.


**Getting Started**
**PetStore - Swagger**

The project has **5** scenarios with **19** steps. Which is located in Pet.feature files:  
[/TestAssignmentLeasePlan/src/test/resources/Features/Pet.feature]

**Scenario: Verify if Order can placed - Positive TestCase** 
 - By running this test place AddOrder will be created new Order and verified the complete API response is ok with mentioned other response also **status code -200** , **status -Placed** && **complete - TRUE**.  
- In next step verified  **Verfiy id created by addOrder <id> by using "GetOrderById" API**. In this stpe checked if getOrder by ID is working fine with same Id created by AddOrder - id. 
-  Used the same data set which is present in the feature file. 

**Scenario: Verify if delete order is working - Positive TestCase** 
- In this test case verified if order id is deleted which was created by AddOrder. 
- Verfied the status response 200 after deleted and type showing Unknown. 

**Scenario: Verify a pet request without body with correct uri- Negative test case** 
- In this test verified the status code 400 bad request in response of service. 
- Verified the response with message = error. 

**Scenario: Verify if getOrder is working after deleted by deleteMethod - Negative Testcase** 
- Verifed the status code 404 in this steps
- Verified the ÖrderNot Found error message. 


Total 5 Scenarios will be executed with 19 stepes to verify each step and response codes,status and error messages. Report file can be found in the job artifacts like [here](https://gitlab.com/pujavrma.11/testassignmentleaseplan/-/jobs/727655068/artifacts/browse/public)



