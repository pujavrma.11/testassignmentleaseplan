Feature: Validate AddPetStore API
  I want to use this template for my feature file

  @smokeTestAddPlace
  Scenario Outline: Verify if Order can placed
    Given Add place payload with data  with <id> <petId> <quantitiy>
    When Call "PlaceOrder" with "Post" HTTP request
    Then Check status code 200
    And Check "status" showing "Placed"
    And Check "complete" showing "true"
    And Verfiy id created by addOrder to <id> by using "GetOrderById"

    Examples: 
      | id | petId | quantitiy |
      | 19 |    99 |         2 | 
      | 11 |    13 |        1  |

  @smokeTestDeleteMethod
  Scenario: Verify if delete order is working
    Given Verfiy id created by getOrder to "code" by using "DeleteOrder"
    And Check if "type" showing "unknown"

  @smokeTestNegativeTestCase
  Scenario: Verify a pet request without body with correct uri
    Given the Swagger Petstore API is available with error response code
    Then the requests response will contain the value 'error' for the 'message' field

  @smokeTestNegativeTestcase
  Scenario: Verify if getOrder is working after deleted by deleteMethod
    Given Place URI with orderid
    When id created by addOrder to <id> by using "GetOrderById"
    Then Check Error code
