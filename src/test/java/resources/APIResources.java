package resources;

import io.restassured.response.Response;

//enum is special class in java which has collection of constants and methods.all the resource uri details mention below
public enum APIResources { 
	
	
	PlaceOrder("/store/order"),
	GetOrderById("/store/order"), 
	DeleteOrder("/store/order"); 
	//to return the string to specificationbuilder class
	 public String resource;
	
	APIResources(String resource)
	{  
		this.resource=resource;
	} 
	
	 public String getResource()
	 {
		return resource;
		 
	 }



	}


