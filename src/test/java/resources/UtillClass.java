package resources;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UtillClass { 
public RequestSpecification requestSpecification; 
    //Utility method for request specification,baseuri,contenttype JSON. 
//logged method details for an api
	public RequestSpecification requestSpecification() throws IOException
	{ 

		PrintStream printstream= new PrintStream(new FileOutputStream("logging.txt"));
		requestSpecification =new RequestSpecBuilder().setBaseUri(getGlobalValue("BaseUrl")) 
				.addFilter(RequestLoggingFilter.logRequestTo(printstream))
				.addFilter(ResponseLoggingFilter.logResponseTo(printstream))
				.setContentType(ContentType.JSON).build();
				return requestSpecification; 
	}
//properties file path for baseUri
	public static String getGlobalValue(String key) throws IOException 
	{
		Properties prop= new Properties();
		FileInputStream fis= new FileInputStream("src/test/java/resources/global.properties"); 
	    prop.load(fis);
	    String s1=prop.getProperty(key);
	  	return prop.getProperty(key); 
		//getGlobalValue("BaseUrl")
		//("https://petstore.swagger.io/v2")
		}
	
	//JSON extraction in string
	public String getJsonPath(Response response,String key)
	{ 
		String str=response.asString();
		JsonPath js= new JsonPath(str);
		return js.get(key).toString();
		
		
	} 
	

	
}
