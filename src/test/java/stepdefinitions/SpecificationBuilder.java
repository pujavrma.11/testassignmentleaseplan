package stepdefinitions;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.junit.Assert;
import resources.APIResources;
import resources.TestData;
import resources.UtillClass;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class SpecificationBuilder extends UtillClass {
	RequestSpecification requestSpecification;
	ResponseSpecification responseSpecification;
	Response response;
	RequestSpecification requestSpecification1;
	JsonPath js;
	TestData td = new TestData();
	static int OrderId;

	// In this test, we are going to test a Post API and validate the Response
	@Given("Add place payload with data  with {int} {int} {int}")
	public void add_place_payload_with_data_with(int id, int petId, int quantity) throws IOException {

		requestSpecification1 = given().spec(requestSpecification()).body(td.PlaceOrder(id, petId, quantity));
	}

	// Code for method calling from UtillClass
	@When("Call {string} with {string} HTTP request")
	public void call_with_HTTP_request(String resource, String method) {
		APIResources apiresource = APIResources.valueOf(resource);
		if (method.equalsIgnoreCase("POST"))
			response = requestSpecification1.when().post(apiresource.getResource());
		else if (method.equalsIgnoreCase("GET"))
			response = requestSpecification1.when().get(apiresource.getResource());
		else if (method.equalsIgnoreCase("DELETE"))
			response = requestSpecification.when().delete(apiresource.getResource());
	}

	// Checking status of post Request for the status code 200
	@Then("Check status code {int}")
	public void check_status_code(Integer int1) {
		assertEquals(response.getStatusCode(), 200);
	}

	// Verifying string contains the "Complete = true" in response of addOrder
	// service
	@And("Check {string} showing {string}")
	public void check_showing(String keyValue, String Expectedvalue) {
		assertEquals(getJsonPath(response, "complete"), "true");
	}

	// In this test, we are going to test a Get API from id which is added by Post
	// API and validate the Response

	@And("Verfiy id created by addOrder to {int} by using {string}")
	public void verfiy_id_created_by_addOrder_to_by_using(Integer expectedId, String resource) throws IOException {
		OrderId = Integer.parseInt(getJsonPath(response, "id"));
		// method call requestSpecification() from utilityClass
		requestSpecification = given().spec(requestSpecification()).pathParam("id", OrderId);
		call_with_HTTP_request(resource, "GET/{OrderId}");
		int ActualpetId = Integer.parseInt(getJsonPath(response, "id"));
		assertEquals(ActualpetId, OrderId);
	}

	// In this test, we are going to test a Delete method and deleted the id is
	// created by Post method.
	@Given("Verfiy id created by getOrder to {string} by using {string}")
	public void verfiy_id_created_by_getOrder_to_by_using(String resource, String expectedOutcome) throws IOException { // given().log().all().spec(requestSpecification());
		requestSpecification = given().spec(requestSpecification());
		response = requestSpecification.delete("/store/order/" + OrderId);
		assertEquals(response.getStatusCode(), 200);
	}

	// Check status if we are getting Unknown in response of API
	@Then("Check if {string} showing {string}")
	public void check_if_showing(String string, String string2) {
		String jsonString = response.asString();
		Assert.assertEquals(jsonString.contains("unknown"), true);
	}

	// In this test, we are going to test a addOrder API without body to check the
	// status code as 400 bad request
	@Given("the Swagger Petstore API is available with error response code")
	public void the_Swagger_Petstore_API_is_available_with_error_response_code() throws IOException {

		requestSpecification = given().spec(requestSpecification());
		response = requestSpecification.post("/store/order/");
	}

	@Then("the requests response will contain the value {string} for the {string} field")
	public void the_requests_response_will_contain_the_value_for_the_field(String string, String string2) {

		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 400);
		String jsonStrings = response.asString();
		Assert.assertEquals(jsonStrings.contains("error"), true);
	}

	// In this test, we are going to test a getOrder API after deleted the Id by
	// deleted API and verify the status code as bad request.
	@Given("Place URI with orderid")
	public void place_URI_with_orderid() throws IOException {
		requestSpecification = given().spec(requestSpecification());

	}

	@When("id created by addOrder to <id> by using {string}")
	public void id_created_by_addOrder_to_id_by_using(String orderid) {
		response = requestSpecification.get("/store/order/" + OrderId);

	}

	@Then("Check Error code")
	public void check_Error_code() {
		int statusCode = response.getStatusCode();
		Assert.assertEquals(statusCode, 404); 
		String jsonStrings = response.asString();
		Assert.assertEquals(jsonStrings.contains("Order not found"), true);
	}

}
