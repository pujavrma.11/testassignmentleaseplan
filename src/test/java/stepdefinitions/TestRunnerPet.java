package stepdefinitions;


import org.junit.runner.RunWith;
import io.cucumber.junit.*;


@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/Features",glue= {"stepdefinitions"}
,monochrome = true
,strict = true
,plugin= {"json:target/jsonReports/cucumber-report.json"})


public class TestRunnerPet { 

}
 